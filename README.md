### ALL SECRETS IN THIS PROJECT ARE FAKE DUMMY SECRETS ONLY USED TO ILLUSTRATE PIPELINE SECRET DETECTION [CUSTOMIZE RULESETS](https://docs.gitlab.com/user/application_security/secret_detection/pipeline/configure/#customize-analyzer-rulesets) FEATURE.

# Override a rule from default ruleset using a remote ruleset

This project [overrides a rule from the default ruleset configuration](https://docs.gitlab.com/user/application_security/secret_detection/pipeline/configure/#override-a-rule) of pipeline secret detection using a [remote ruleset](https://docs.gitlab.com/user/application_security/secret_detection/pipeline/configure/#with-a-remote-ruleset) to modify the attributes of the secret detected.

The result of running `secret_detection` job in the pipelines of this project is:

1. A *[**remote ruleset**](https://gitlab.com/gitlab-org/security-products/demos/analyzer-configurations/secret-detection/modify-default-ruleset/remote-ruleset/override-rule-ruleset/-/blob/main/.gitlab/secret-detection-ruleset.toml) is used to override attributes of a rule from the [default ruleset](https://gitlab.com/gitlab-org/security-products/analyzers/secrets/-/blob/master/gitleaks.toml).
1. A [secret](./private_key) has modified `description`, `message`, and `name` in the `gl-secret-detection-report.json` file.

*A _remote ruleset_ is a **ruleset configuration file stored outside the current repository**.

To confirm this working as intended, check the [security tab of one of the successful pipelines](https://gitlab.com/gitlab-org/security-products/demos/analyzer-configurations/secret-detection/modify-default-ruleset/remote-ruleset/override-rule-project/-/pipelines/1380733025/security).
